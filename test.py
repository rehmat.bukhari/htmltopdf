<<<<<<< HEAD
=======
# from flask import Flask, request, jsonify, send_file
# from flask_cors import CORS
# import io
# import time
# import logging
# import os
# import shutil
# import threading
# from selenium import webdriver
# from selenium.webdriver.chrome.options import Options
# from PIL import Image
# from reportlab.lib.pagesizes import letter
# from reportlab.pdfgen import canvas
# from pdf_manipulation import *
# import chromedriver_autoinstaller

# app = Flask(__name__)
# CORS(app)

# # Set up logging
# logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s', handlers=[
#     logging.FileHandler("app.log"),
#     logging.StreamHandler()
# ])

# lock = threading.Lock()

# # Ensure directories exist and are cleaned
# def setup_directories(download_folder, output_folder):
#     os.makedirs(download_folder, exist_ok=True)
#     os.makedirs(output_folder, exist_ok=True)
#     shutil.rmtree(download_folder)
#     shutil.rmtree(output_folder)
#     os.makedirs(download_folder, exist_ok=True)
#     os.makedirs(output_folder, exist_ok=True)

# def generate_pdf_from_url(download_folder, url, include_footer, store_name, visit_ref):
#     logging.info(f"Generating PDF from URL: {url}")
#     chrome_options = Options()
#     chrome_options.add_argument("--headless")
#     chrome_options.add_argument("--disable-gpu")
#     chrome_options.add_argument("--no-sandbox")
#     chrome_options.add_argument("--disable-dev-shm-usage")
    
#     # Automatically install and setup ChromeDriver
#     chromedriver_autoinstaller.install()

#     try:
#         driver = webdriver.Chrome(options=chrome_options)
#     except Exception as e:
#         logging.error(f"Failed to initialize Chrome WebDriver: {e}")
#         raise

#     try:
#         driver.get(url)
#         time.sleep(10)  # Wait for the page to load fully

#         # Disable overflow and dynamic behavior for screenshots
#         driver.execute_script("document.body.style.overflow = 'hidden';")

#         # Get dynamic dimensions of the page
#         total_height = driver.execute_script("return document.body.scrollHeight")
#         total_width = driver.execute_script("return Math.max(document.body.scrollWidth, document.documentElement.scrollWidth);")
        
#         # Set the window size dynamically based on actual dimensions of the webpage
#         driver.set_window_size(total_width, total_height)

#         # Take screenshot after setting the window size
#         screenshot = driver.get_screenshot_as_png()

#         # Save the screenshot as an image using PIL
#         screenshot_image = Image.open(io.BytesIO(screenshot))
#         timestamp = int(time.time())
#         screenshot_file_path = os.path.join(download_folder, f"Generated_pdf_{timestamp}.png")
#         screenshot_image.save(screenshot_file_path)

#         # If footer is to be included, we calculate the height needed
#         footer_height = 40 if include_footer else 0
#         pdf_height = total_height + footer_height

#         # Create PDF from the image
#         pdf_file_path = os.path.join(download_folder, f"Generated_pdf_{timestamp}.pdf")
#         canvas_obj = canvas.Canvas(pdf_file_path, pagesize=(total_width, pdf_height))
#         canvas_obj.drawImage(screenshot_file_path, 0, footer_height, width=total_width, height=total_height)
#         canvas_obj.save()

#     except Exception as e:
#         logging.error(f"Error during PDF generation: {e}")
#         driver.quit()
#         raise
#     driver.quit()
#     logging.info("PDF generated successfully")
#     return pdf_file_path

# @app.route('/generate_pdf', methods=['POST'])
# def generate_pdf():
#     # Get the URL and footer information from the request
#     url = request.json.get('url')
#     include_footer = request.json.get('include_footer', True)
#     store_name = request.json.get('store_name', '')
#     visit_ref = request.json.get('visit_ref', '')

#     if not url:
#         error_message = 'URL not provided in the request body'
#         logging.error(error_message)
#         print(error_message)
#         return jsonify({'error': error_message}), 400

#     try:
#         hex_color = '#0000FF'  # Example hex color for blue ribbon
#         lower_hsv = np.array([90, 50, 50])  # Example lower HSV bound for blue
#         upper_hsv = np.array([130, 255, 255])  # Example upper HSV bound for blue

#         output_folder = r'output'
#         download_folder = r'download'

#         with lock:
#             setup_directories(download_folder, output_folder)
#             pdf_file_path = generate_pdf_from_url(download_folder, url, include_footer, store_name, visit_ref)
#             pdf_output_path = os.path.join(output_folder, os.path.basename(pdf_file_path))
#             split_pdf_by_ribbon_color(pdf_file_path, pdf_output_path, output_folder, lower_hsv=lower_hsv, upper_hsv=upper_hsv, store=store_name, visit_ref=visit_ref )

#         return send_file(pdf_output_path, as_attachment=True)
#     except Exception as e:
#         error_message = f"An error occurred: {str(e)}"
#         logging.error(error_message)
#         print(error_message)
#         return jsonify({'error': error_message}), 500

# if __name__ == '__main__':
#     logging.info("Starting the Flask server...")
#     print("Starting the Flask server...")
#     app.run(debug=True, port=4210, host='0.0.0.0')
# from flask import Flask, request, jsonify, send_file
# from flask_cors import CORS
# import io
# import time
# import logging
# import os
# import shutil
# import threading
# from selenium import webdriver
# from selenium.webdriver.chrome.options import Options
# from PIL import Image
# from reportlab.lib.pagesizes import letter
# from reportlab.pdfgen import canvas
# from pdf_manipulation import *
# import chromedriver_autoinstaller

# app = Flask(__name__)
# CORS(app)

# # Set up logging
# logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s', handlers=[
#     logging.FileHandler("app.log"),
#     logging.StreamHandler()
# ])

# lock = threading.Lock()

# # Ensure directories exist and are cleaned
# def setup_directories(download_folder, output_folder):
#     os.makedirs(download_folder, exist_ok=True)
#     os.makedirs(output_folder, exist_ok=True)
#     shutil.rmtree(download_folder)
#     shutil.rmtree(output_folder)
#     os.makedirs(download_folder, exist_ok=True)
#     os.makedirs(output_folder, exist_ok=True)

# def generate_pdf_from_url(download_folder, url, include_footer, store_name, visit_ref):
#     logging.info(f"Generating PDF from URL: {url}")
#     chrome_options = Options()
#     chrome_options.add_argument("--headless")
#     chrome_options.add_argument("--disable-gpu")
#     chrome_options.add_argument("--no-sandbox")
#     chrome_options.add_argument("--disable-dev-shm-usage")
    
#     # Automatically install and setup ChromeDriver
#     chromedriver_autoinstaller.install()

#     try:
#         driver = webdriver.Chrome(options=chrome_options)
#     except Exception as e:
#         logging.error(f"Failed to initialize Chrome WebDriver: {e}")
#         raise

#     try:
#         driver.get(url)
#         time.sleep(10)  # Wait for the page to load fully

#         # Disable overflow and dynamic behavior for screenshots
#         driver.execute_script("document.body.style.overflow = 'hidden';")

#         # Get dynamic dimensions of the page
#         total_height = driver.execute_script("return document.body.scrollHeight")
#         total_width = driver.execute_script("return Math.max(document.body.scrollWidth, document.documentElement.scrollWidth);")
        
#         # Set the window size dynamically based on actual dimensions of the webpage
#         driver.set_window_size(total_width, total_height)

#         # Take screenshot after setting the window size
#         screenshot = driver.get_screenshot_as_png()

#         # Save the screenshot as an image using PIL
#         screenshot_image = Image.open(io.BytesIO(screenshot))
#         timestamp = int(time.time())
#         screenshot_file_path = os.path.join(download_folder, f"Generated_pdf_{timestamp}.png")
#         screenshot_image.save(screenshot_file_path)

#         # If footer is to be included, we calculate the height needed
#         footer_height = 40 if include_footer else 0
#         pdf_height = total_height + footer_height

#         # Create PDF from the image
#         pdf_file_path = os.path.join(download_folder, f"Generated_pdf_{timestamp}.pdf")
#         canvas_obj = canvas.Canvas(pdf_file_path, pagesize=(total_width, pdf_height))
#         canvas_obj.drawImage(screenshot_file_path, 0, footer_height, width=total_width, height=total_height)
#         canvas_obj.save()

#     except Exception as e:
#         logging.error(f"Error during PDF generation: {e}")
#         driver.quit()
#         raise
#     driver.quit()
#     logging.info("PDF generated successfully")
#     return pdf_file_path

# @app.route('/generate_pdf', methods=['POST'])
# def generate_pdf():
#     # Get the URL and footer information from the request
#     url = request.json.get('url')
#     include_footer = request.json.get('include_footer', True)
#     store_name = request.json.get('store_name', '')
#     visit_ref = request.json.get('visit_ref', '')

#     if not url:
#         error_message = 'URL not provided in the request body'
#         logging.error(error_message)
#         print(error_message)
#         return jsonify({'error': error_message}), 400

#     try:
#         hex_color = '#0000FF'  # Example hex color for blue ribbon
#         lower_hsv = np.array([90, 50, 50])  # Example lower HSV bound for blue
#         upper_hsv = np.array([130, 255, 255])  # Example upper HSV bound for blue

#         output_folder = r'output'
#         download_folder = r'download'

#         with lock:
#             setup_directories(download_folder, output_folder)
#             pdf_file_path = generate_pdf_from_url(download_folder, url, include_footer, store_name, visit_ref)
#             pdf_output_path = os.path.join(output_folder, os.path.basename(pdf_file_path))
#             split_pdf_by_ribbon_color(pdf_file_path, pdf_output_path, output_folder, lower_hsv=lower_hsv, upper_hsv=upper_hsv, store=store_name, visit_ref=visit_ref )

#         return send_file(pdf_output_path, as_attachment=True)
#     except Exception as e:
#         error_message = f"An error occurred: {str(e)}"
#         logging.error(error_message)
#         print(error_message)
#         return jsonify({'error': error_message}), 500

# if __name__ == '__main__':
#     logging.info("Starting the Flask server...")
#     print("Starting the Flask server...")
#     app.run(debug=True, port=4210, host='0.0.0.0')


# from flask import Flask, request, jsonify, send_file
# from flask_cors import CORS
# import io
# import time
# import logging
# import os
# import shutil
# import threading
# from selenium import webdriver
# from selenium.webdriver.chrome.options import Options
# from PIL import Image
# from reportlab.lib.pagesizes import letter
# from reportlab.pdfgen import canvas
# from pdf_manipulation import *
# import chromedriver_autoinstaller
# from concurrent.futures import ThreadPoolExecutor
# from flask import copy_current_request_context

# app = Flask(__name__)
# CORS(app)

# # Set up logging
# logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s', handlers=[
#     logging.FileHandler("app.log"),
#     logging.StreamHandler()
# ])

# lock = threading.Lock()

# # Initialize ThreadPoolExecutor for multithreading
# executor = ThreadPoolExecutor(max_workers=5)  # Adjust max_workers according to system resources

# # Ensure directories exist and are cleaned
# def setup_directories(download_folder, output_folder):
#     os.makedirs(download_folder, exist_ok=True)
#     os.makedirs(output_folder, exist_ok=True)
#     shutil.rmtree(download_folder)
#     shutil.rmtree(output_folder)
#     os.makedirs(download_folder, exist_ok=True)
#     os.makedirs(output_folder, exist_ok=True)

# def generate_pdf_from_url(download_folder, url, include_footer, store_name, visit_ref):
#     logging.info(f"Generating PDF from URL: {url}")
#     chrome_options = Options()
#     chrome_options.add_argument("--headless")
#     chrome_options.add_argument("--disable-gpu")
#     chrome_options.add_argument("--no-sandbox")
#     chrome_options.add_argument("--disable-dev-shm-usage")
    
#     # Automatically install and setup ChromeDriver
#     chromedriver_autoinstaller.install()

#     try:
#         driver = webdriver.Chrome(options=chrome_options)
#     except Exception as e:
#         logging.error(f"Failed to initialize Chrome WebDriver: {e}")
#         raise

#     try:
#         driver.get(url)
#         time.sleep(15)  # Wait for the page to load fully

#         # Disable overflow and dynamic behavior for screenshots
#         driver.execute_script("document.body.style.overflow = 'hidden';")

#         # Get dynamic dimensions of the page
#         total_height = driver.execute_script("return document.body.scrollHeight")
#         total_width = driver.execute_script("return Math.max(document.body.scrollWidth, document.documentElement.scrollWidth);")
        
#         # Set the window size dynamically based on actual dimensions of the webpage
#         driver.set_window_size(total_width, total_height)

#         # Take screenshot after setting the window size
#         screenshot = driver.get_screenshot_as_png()

#         # Save the screenshot as an image using PIL
#         screenshot_image = Image.open(io.BytesIO(screenshot))
#         timestamp = int(time.time())
#         screenshot_file_path = os.path.join(download_folder, f"Generated_pdf_{timestamp}.png")
#         screenshot_image.save(screenshot_file_path)

#         # If footer is to be included, we calculate the height needed
#         footer_height = 40 if include_footer else 0
#         pdf_height = total_height + footer_height

#         # Create PDF from the image
#         pdf_file_path = os.path.join(download_folder, f"Generated_pdf_{timestamp}.pdf")
#         canvas_obj = canvas.Canvas(pdf_file_path, pagesize=(total_width, pdf_height))
#         canvas_obj.drawImage(screenshot_file_path, 0, footer_height, width=total_width, height=total_height)
#         canvas_obj.save()

#     except Exception as e:
#         logging.error(f"Error during PDF generation: {e}")
#         driver.quit()
#         raise
#     driver.quit()
#     logging.info("PDF generated successfully")
#     return pdf_file_path

# @app.route('/generate_pdf', methods=['POST'])
# def generate_pdf():
#     # Copy the request context into the thread
#     @copy_current_request_context
#     def process_pdf():
#         try:
#             output_folder = r'output'
#             download_folder = r'download'

#             setup_directories(download_folder, output_folder)

#             url = request.json.get('url')
#             include_footer = request.json.get('include_footer', True)
#             store_name = request.json.get('store_name', '')
#             visit_ref = request.json.get('visit_ref', '')

#             if not url:
#                 return jsonify({'error': 'URL not provided in the request body'}), 400

#             hex_color = '#0000FF'  # Example hex color for blue ribbon
#             lower_hsv = np.array([90, 50, 50])  # Example lower HSV bound for blue
#             upper_hsv = np.array([130, 255, 255])  # Example upper HSV bound for blue

#             # Generate and split the PDF
#             pdf_file_path = generate_pdf_from_url(download_folder, url, include_footer, store_name, visit_ref)
#             pdf_output_path = os.path.join(output_folder, os.path.basename(pdf_file_path))
#             split_pdf_by_ribbon_color(pdf_file_path, pdf_output_path, output_folder, lower_hsv=lower_hsv, upper_hsv=upper_hsv, store=store_name, visit_ref=visit_ref)
#             return send_file(pdf_output_path, as_attachment=True)

#         except Exception as e:
#             error_message = f"An error occurred: {str(e)}"
#             logging.error(error_message)
#             return jsonify({'error': error_message}), 500

#     # Submit the PDF generation process to a thread and return the result
#     future = executor.submit(process_pdf)
#     return future.result()

# if __name__ == '__main__':
#     logging.info("Starting the Flask server...")
#     app.run(debug=True, port=4210, host='0.0.0.0')
>>>>>>> e0b73e296f8de5f2d6c99982e4166cce87d582c8


from flask import Flask, request, jsonify, send_file
from flask_cors import CORS
import io
import time
import logging
import os
import shutil
import threading
<<<<<<< HEAD
import numpy as np
=======
>>>>>>> e0b73e296f8de5f2d6c99982e4166cce87d582c8
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from PIL import Image
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
<<<<<<< HEAD
from pdf_manipulation import *  # Import your manipulation functions
# import chromedriver_autoinstaller
=======
from pdf_manipulation import *
import chromedriver_autoinstaller
>>>>>>> e0b73e296f8de5f2d6c99982e4166cce87d582c8
from concurrent.futures import ThreadPoolExecutor
from flask import copy_current_request_context

app = Flask(__name__)
CORS(app)

# Set up logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s', handlers=[
    logging.FileHandler("app.log"),
    logging.StreamHandler()
])

lock = threading.Lock()

# Initialize ThreadPoolExecutor for multithreading
<<<<<<< HEAD
executor = ThreadPoolExecutor(max_workers=5)
=======
executor = ThreadPoolExecutor(max_workers=5)  # Adjust max_workers according to system resources
>>>>>>> e0b73e296f8de5f2d6c99982e4166cce87d582c8

# Cleanup function to remove temporary directories after a certain time
def cleanup_directory(directory, delay=20):
    time.sleep(delay)
    if os.path.exists(directory):
        shutil.rmtree(directory)
        logging.info(f"Temporary folder {directory} has been removed.")

# Ensure unique directories for each request
def setup_unique_directories(store_name, visit_ref):
    timestamp = int(time.time())
    base_folder = f"{store_name}_{visit_ref}_{timestamp}"
    unique_folder = os.path.join(os.getcwd(), base_folder)
    
    download_folder = os.path.join(unique_folder, 'download')
    output_folder = os.path.join(unique_folder, 'output')

    os.makedirs(download_folder, exist_ok=True)
    os.makedirs(output_folder, exist_ok=True)

    return unique_folder, download_folder, output_folder

<<<<<<< HEAD
# Function to generate a PDF from a URL screenshot
=======
>>>>>>> e0b73e296f8de5f2d6c99982e4166cce87d582c8
def generate_pdf_from_url(download_folder, url, include_footer, store_name, visit_ref):
    logging.info(f"Generating PDF from URL: {url}")
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--no-sandbox")
<<<<<<< HEAD
    # chrome_options.add_argument("--disable-dev-shm-usage")
    
    # Automatically install and setup ChromeDriver
    # chromedriver_autoinstaller.install()
=======
    chrome_options.add_argument("--disable-dev-shm-usage")
    
    # Automatically install and setup ChromeDriver
    chromedriver_autoinstaller.install()
>>>>>>> e0b73e296f8de5f2d6c99982e4166cce87d582c8

    try:
        driver = webdriver.Chrome(options=chrome_options)
    except Exception as e:
        logging.error(f"Failed to initialize Chrome WebDriver: {e}")
        raise

    try:
        driver.get(url)
<<<<<<< HEAD
        time.sleep(30)  # Wait for the page to load fully
=======
        time.sleep(10)  # Wait for the page to load fully
>>>>>>> e0b73e296f8de5f2d6c99982e4166cce87d582c8

        # Disable overflow and dynamic behavior for screenshots
        driver.execute_script("document.body.style.overflow = 'hidden';")

        # Get dynamic dimensions of the page
<<<<<<< HEAD
        # total_height = driver.execute_script("return document.body.scrollHeight")
        total_height = driver.execute_script("return Math.max(document.body.scrollHeight, document.documentElement.clientHeight, document.documentElement.offsetHeight);")
=======
        total_height = driver.execute_script("return document.body.scrollHeight")
>>>>>>> e0b73e296f8de5f2d6c99982e4166cce87d582c8
        total_width = driver.execute_script("return Math.max(document.body.scrollWidth, document.documentElement.scrollWidth);")
        
        # Set the window size dynamically based on actual dimensions of the webpage
        driver.set_window_size(total_width, total_height)

        # Take screenshot after setting the window size
        screenshot = driver.get_screenshot_as_png()

        # Save the screenshot as an image using PIL
        screenshot_image = Image.open(io.BytesIO(screenshot))
        timestamp = int(time.time())
        screenshot_file_path = os.path.join(download_folder, f"Generated_pdf_{timestamp}.png")
        screenshot_image.save(screenshot_file_path)

        # If footer is to be included, we calculate the height needed
        footer_height = 40 if include_footer else 0
        pdf_height = total_height + footer_height

        # Create PDF from the image
        pdf_file_path = os.path.join(download_folder, f"Generated_pdf_{timestamp}.pdf")
        canvas_obj = canvas.Canvas(pdf_file_path, pagesize=(total_width, pdf_height))
        canvas_obj.drawImage(screenshot_file_path, 0, footer_height, width=total_width, height=total_height)
        canvas_obj.save()

    except Exception as e:
        logging.error(f"Error during PDF generation: {e}")
        driver.quit()
        raise
    driver.quit()
    logging.info("PDF generated successfully")
    return pdf_file_path

@app.route('/generate_pdf', methods=['POST'])
def generate_pdf():
    # Copy the request context into the thread
    @copy_current_request_context
    def process_pdf():
        try:
            url = request.json.get('url')
            include_footer = request.json.get('include_footer', True)
            store_name = request.json.get('store_name', 'store')
            visit_ref = request.json.get('visit_ref', 'visit')

            if not url:
                return jsonify({'error': 'URL not provided in the request body'}), 400

            # Create unique directories for each request
            unique_folder, download_folder, output_folder = setup_unique_directories(store_name, visit_ref)

            hex_color = '#0000FF'  # Example hex color for blue ribbon
            lower_hsv = np.array([90, 50, 50])  # Example lower HSV bound for blue
            upper_hsv = np.array([130, 255, 255])  # Example upper HSV bound for blue

            # Generate and split the PDF
            pdf_file_path = generate_pdf_from_url(download_folder, url, include_footer, store_name, visit_ref)
            pdf_output_path = os.path.join(output_folder, os.path.basename(pdf_file_path))
<<<<<<< HEAD
            
            # Call split_pdf_by_ribbon_color to add footer and split by ribbon color
=======
>>>>>>> e0b73e296f8de5f2d6c99982e4166cce87d582c8
            split_pdf_by_ribbon_color(pdf_file_path, pdf_output_path, output_folder, lower_hsv=lower_hsv, upper_hsv=upper_hsv, store=store_name, visit_ref=visit_ref)

            # Schedule cleanup of the unique directory after 20 seconds
            threading.Thread(target=cleanup_directory, args=(unique_folder,)).start()

            return send_file(pdf_output_path, as_attachment=True)

        except Exception as e:
            error_message = f"An error occurred: {str(e)}"
            logging.error(error_message)
            return jsonify({'error': error_message}), 500

    # Submit the PDF generation process to a thread and return the result
    future = executor.submit(process_pdf)
    return future.result()

if __name__ == '__main__':
    logging.info("Starting the Flask server...")
<<<<<<< HEAD
    app.run(debug=True, port=4000, host='0.0.0.0')
=======
    app.run(debug=True, port=4210, host='0.0.0.0')

>>>>>>> e0b73e296f8de5f2d6c99982e4166cce87d582c8
