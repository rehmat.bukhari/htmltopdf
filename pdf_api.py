<<<<<<< HEAD
# import os
# import time
# import shutil
# import logging
# from fastapi import FastAPI, HTTPException, BackgroundTasks
# from fastapi.responses import FileResponse
# from pydantic import BaseModel
# from playwright.sync_api import sync_playwright
=======


# from flask import Flask, request, jsonify, send_file
# from flask_cors import CORS
# import io
# import time
# import logging
# import os
# import shutil
# import threading
# import numpy as np
# from selenium import webdriver
# from selenium.webdriver.chrome.options import Options
# from PIL import Image
# from reportlab.lib.pagesizes import letter
# from reportlab.pdfgen import canvas
# from pdf_manipulation import split_pdf_by_ribbon_color  # Import your manipulation functions
# import chromedriver_autoinstaller
# from concurrent.futures import ThreadPoolExecutor
# from flask import copy_current_request_context

# app = Flask(__name__)
# CORS(app)

# # Set up logging
# logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s', handlers=[
#     logging.FileHandler("app.log"),
#     logging.StreamHandler()
# ])

# lock = threading.Lock()

# # Initialize ThreadPoolExecutor for multithreading
# executor = ThreadPoolExecutor(max_workers=5)

# # Cleanup function to remove temporary directories after a certain time
# def cleanup_directory(directory, delay=20):
#     time.sleep(delay)
#     if os.path.exists(directory):
#         shutil.rmtree(directory)
#         logging.info(f"Temporary folder {directory} has been removed.")

# # Ensure unique directories for each request
# def setup_unique_directories(store_name, visit_ref):
#     timestamp = int(time.time())
#     base_folder = f"{store_name}_{visit_ref}_{timestamp}"
#     unique_folder = os.path.join(os.getcwd(), base_folder)
    
#     download_folder = os.path.join(unique_folder, 'download')
#     output_folder = os.path.join(unique_folder, 'output')

#     os.makedirs(download_folder, exist_ok=True)
#     os.makedirs(output_folder, exist_ok=True)

#     return unique_folder, download_folder, output_folder
# def generate_pdf_from_url(download_folder, url, include_footer, store_name, visit_ref):
#     logging.info(f"Generating PDF from URL: {url}")
#     chrome_options = Options()
#     chrome_options.add_argument("--headless")
#     chrome_options.add_argument("--disable-gpu")
#     chrome_options.add_argument("--no-sandbox")
#     chrome_options.add_argument("--disable-dev-shm-usage")

#     # Automatically install and setup ChromeDriver
#     chromedriver_autoinstaller.install()

#     try:
#         driver = webdriver.Chrome(options=chrome_options)
#         driver.get(url)
#         time.sleep(2)  # Allow the page to start loading

#         # Get the full page dimensions
#         total_width = driver.execute_script("return document.documentElement.scrollWidth")
#         total_height = driver.execute_script("return document.documentElement.scrollHeight")

#         # Set the browser window size to match the full page dimensions
#         driver.set_window_size(total_width, total_height)

#         # Take a full-page screenshot
#         screenshot = driver.get_screenshot_as_png()

#         # Save the screenshot as an image using PIL
#         screenshot_image = Image.open(io.BytesIO(screenshot))
#         timestamp = int(time.time())
#         screenshot_file_path = os.path.join(download_folder, f"Generated_pdf_{timestamp}.png")
#         screenshot_image.save(screenshot_file_path)

#         # Adjust PDF height to add a footer if needed
#         footer_height = 40 if include_footer else 0
#         pdf_height = screenshot_image.height + footer_height

#         # Create PDF from the image
#         pdf_file_path = os.path.join(download_folder, f"Generated_pdf_{timestamp}.pdf")
#         canvas_obj = canvas.Canvas(pdf_file_path, pagesize=(screenshot_image.width, pdf_height))
#         canvas_obj.drawImage(screenshot_file_path, 0, footer_height, width=screenshot_image.width, height=screenshot_image.height)
#         canvas_obj.save()

#     except Exception as e:
#         logging.error(f"Error during PDF generation: {e}")
#         driver.quit()
#         raise
#     finally:
#         driver.quit()

#     logging.info("PDF generated successfully")
#     return pdf_file_path



# @app.route('/generate_pdf', methods=['POST'])
# def generate_pdf():
#     # Copy the request context into the thread
#     @copy_current_request_context
#     def process_pdf():
#         try:
#             url = request.json.get('url')
#             include_footer = request.json.get('include_footer', True)
#             store_name = request.json.get('store_name', 'store')
#             visit_ref = request.json.get('visit_ref', 'visit')

#             if not url:
#                 return jsonify({'error': 'URL not provided in the request body'}), 400

#             # Create unique directories for each request
#             unique_folder, download_folder, output_folder = setup_unique_directories(store_name, visit_ref)

#             hex_color = '#0000FF'  # Example hex color for blue ribbon
#             lower_hsv = np.array([90, 50, 50])  # Example lower HSV bound for blue
#             upper_hsv = np.array([130, 255, 255])  # Example upper HSV bound for blue

#             # Generate and split the PDF
#             pdf_file_path = generate_pdf_from_url(download_folder, url, include_footer, store_name, visit_ref)
#             pdf_output_path = os.path.join(output_folder, os.path.basename(pdf_file_path))
            
#             # Call split_pdf_by_ribbon_color to add footer and split by ribbon color
#             split_pdf_by_ribbon_color(pdf_file_path, pdf_output_path, output_folder, lower_hsv=lower_hsv, upper_hsv=upper_hsv, store=store_name, visit_ref=visit_ref)

#             # Schedule cleanup of the unique directory after 20 seconds
#             threading.Thread(target=cleanup_directory, args=(unique_folder,)).start()

#             return send_file(pdf_output_path, as_attachment=True)

#         except Exception as e:
#             error_message = f"An error occurred: {str(e)}"
#             logging.error(error_message)
#             return jsonify({'error': error_message}), 500

#     # Submit the PDF generation process to a thread and return the result
#     future = executor.submit(process_pdf)
#     return future.result()

# if __name__ == '__main__':
#     logging.info("Starting the Flask server...")
#     app.run(debug=True, port=4210, host='0.0.0.0')




from flask import Flask, request, jsonify, send_file
from flask_cors import CORS
import io
import time
import logging
import os
import shutil
import threading
import numpy as np
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from PIL import Image
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from pdf_manipulation import split_pdf_by_ribbon_color  # Import your manipulation functions
import chromedriver_autoinstaller
from concurrent.futures import ThreadPoolExecutor
from flask import copy_current_request_context
>>>>>>> e0b73e296f8de5f2d6c99982e4166cce87d582c8

# app = FastAPI()

# # Set up logging
# logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s", handlers=[
#     logging.FileHandler("app.log"),
#     logging.StreamHandler()
# ])

# # Request body schema
# class URLRequest(BaseModel):
#     url: str
#     store_name: str
#     visit_ref: str


# def cleanup_folder(folder_path: str, delay: int = 20):
#     """Background task to delete the folder after a certain delay."""
#     time.sleep(delay)
#     if os.path.exists(folder_path):
#         shutil.rmtree(folder_path)
#         logging.info(f"Temporary folder {folder_path} has been removed.")


# def setup_unique_directories(store_name: str, visit_ref: str):
#     """Set up unique directories for processing each request."""
#     timestamp = int(time.time())
#     base_folder = f"{store_name}_{visit_ref}_{timestamp}"
#     unique_folder = os.path.join(os.getcwd(), base_folder)
#     download_folder = os.path.join(unique_folder, 'download')

#     os.makedirs(download_folder, exist_ok=True)

#     return unique_folder, download_folder


# def generate_pdf_from_url(download_folder: str, url: str):
#     """Generate a proper vector-based PDF from the URL."""
#     logging.info(f"Generating PDF from URL: {url}")
#     with sync_playwright() as p:
#         browser = p.chromium.launch(headless=True)
#         context = browser.new_context()
#         page = context.new_page()

#         # Navigate to the URL
#         page.goto(url, wait_until="domcontentloaded")
#         time.sleep(15)  # Allow the page to fully load

#         # Save the webpage as a PDF directly
#         pdf_file_path = os.path.join(download_folder, "generated.pdf")
#         page.pdf(path=pdf_file_path, format="A4", print_background=True)

#         browser.close()

#     return pdf_file_path


# @app.post("/generate_pdf/")
# def generate_pdf(request: URLRequest, background_tasks: BackgroundTasks):
#     try:
#         # Create unique directories for the request
#         unique_folder, download_folder = setup_unique_directories(request.store_name, request.visit_ref)

#         # Generate the initial PDF and save it in the download folder
#         pdf_file_path = generate_pdf_from_url(download_folder, request.url)

#         # Schedule cleanup of the unique folder after processing
#         background_tasks.add_task(cleanup_folder, unique_folder)

#         # Serve the generated PDF from the download folder
#         return FileResponse(pdf_file_path, media_type="application/pdf", filename="Generated_PDF.pdf")

#     except Exception as e:
#         logging.error(f"Error generating PDF: {e}")
#         raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")


# @app.on_event("shutdown")
# def clean_remaining_folders():
#     """Optional cleanup for debugging or testing in case of errors."""
#     for folder in os.listdir("."):
#         if folder.startswith("output_"):
#             shutil.rmtree(folder)


# if __name__ == "__main__":
#     import uvicorn
#     logging.info("Starting the FastAPI server...")
#     uvicorn.run(app, host="0.0.0.0", port=4000)

import os
import time
import shutil
import logging
from fastapi import FastAPI, HTTPException, BackgroundTasks
from fastapi.responses import FileResponse
from pydantic import BaseModel
from playwright.sync_api import sync_playwright
from PyPDF2 import PdfReader, PdfWriter
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from io import BytesIO

app = FastAPI()

# Set up logging
logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s", handlers=[
    logging.FileHandler("app.log"),
    logging.StreamHandler()
])

<<<<<<< HEAD
# Request body schema
class URLRequest(BaseModel):
    url: str
    store_name: str
    visit_ref: str


def cleanup_folder(folder_path: str, delay: int = 20):
    """Background task to delete the folder after a certain delay."""
    time.sleep(delay)
    if os.path.exists(folder_path):
        shutil.rmtree(folder_path)
        logging.info(f"Temporary folder {folder_path} has been removed.")


def setup_unique_directories(store_name: str, visit_ref: str):
    """Set up unique directories for processing each request."""
    timestamp = int(time.time())
    base_folder = f"{store_name}_{visit_ref}_{timestamp}"
    unique_folder = os.path.join(os.getcwd(), base_folder)
    download_folder = os.path.join(unique_folder, 'download')

    os.makedirs(download_folder, exist_ok=True)

    return unique_folder, download_folder


def generate_pdf_from_url(download_folder: str, url: str):
    """Generate a proper vector-based PDF from the URL."""
    logging.info(f"Generating PDF from URL: {url}")
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=True)
        context = browser.new_context()
        page = context.new_page()

        # Navigate to the URL
        page.goto(url, wait_until="domcontentloaded")
        time.sleep(15)  # Allow the page to fully load

        # Save the webpage as a PDF directly
        pdf_file_path = os.path.join(download_folder, "generated.pdf")
        page.pdf(path=pdf_file_path, format="A4", print_background=True)

        browser.close()

    return pdf_file_path


def add_footer_to_pdf(input_pdf_path: str, output_pdf_path: str, store_name: str, visit_ref: str):
    """Add a footer with store name, visit reference, and page number to each page of the PDF."""
    reader = PdfReader(input_pdf_path)
    writer = PdfWriter()

    for page_number, page in enumerate(reader.pages, start=1):
        # Create a new PDF with the footer
        packet = BytesIO()
        can = canvas.Canvas(packet, pagesize=A4)

        # Footer settings
        footer_y_position = 20  # Position near the bottom of the page
        left_margin = 30  # Margin for store name and page number
        right_margin = A4[0] - 150  # Margin for the visit ref on the right

        # Combine store name and page number
        store_with_page = f"Page {page_number} - {store_name}"

        # Add combined store name and page number (left) and visit ref (right)
        can.setFont("Helvetica", 10)
        can.drawString(left_margin, footer_y_position, store_with_page)  # Store name with page number
        can.drawString(right_margin, footer_y_position, f"{visit_ref}")  # Visit ref

        # Finalize the footer canvas
        can.save()

        # Merge the footer with the original page
        packet.seek(0)
        footer_pdf = PdfReader(packet)
        footer_page = footer_pdf.pages[0]
        page.merge_page(footer_page)
        writer.add_page(page)

    # Save the output PDF
    with open(output_pdf_path, "wb") as output_file:
        writer.write(output_file)



@app.post("/generate_pdf/")
def generate_pdf(request: URLRequest, background_tasks: BackgroundTasks):
    try:
        # Create unique directories for the request
        unique_folder, download_folder = setup_unique_directories(request.store_name, request.visit_ref)

        # Generate the initial PDF and save it in the download folder
        pdf_file_path = generate_pdf_from_url(download_folder, request.url)

        # Add footer to the generated PDF
        output_pdf_path = os.path.join(download_folder, "generated_with_footer.pdf")
        add_footer_to_pdf(pdf_file_path, output_pdf_path, request.store_name, request.visit_ref)

        # Schedule cleanup of the unique folder after processing
        background_tasks.add_task(cleanup_folder, unique_folder)

        # Serve the processed PDF with footer from the download folder
        return FileResponse(output_pdf_path, media_type="application/pdf", filename="Generated_PDF_with_Footer.pdf")

    except Exception as e:
        logging.error(f"Error generating PDF: {e}")
        raise HTTPException(status_code=500, detail=f"An error occurred: {str(e)}")


@app.on_event("shutdown")
def clean_remaining_folders():
    """Optional cleanup for debugging or testing in case of errors."""
    for folder in os.listdir("."):
        if folder.startswith("output_"):
            shutil.rmtree(folder)


if __name__ == "__main__":
    import uvicorn
    logging.info("Starting the FastAPI server...")
    uvicorn.run(app, host="0.0.0.0", port=8089)
=======
lock = threading.Lock()

# Initialize ThreadPoolExecutor for multithreading
executor = ThreadPoolExecutor(max_workers=5)

# Cleanup function to remove temporary directories after a certain time
def cleanup_directory(directory, delay=20):
    time.sleep(delay)
    if os.path.exists(directory):
        shutil.rmtree(directory)
        logging.info(f"Temporary folder {directory} has been removed.")

# Ensure unique directories for each request
def setup_unique_directories(store_name, visit_ref):
    timestamp = int(time.time())
    base_folder = f"{store_name}_{visit_ref}_{timestamp}"
    unique_folder = os.path.join(os.getcwd(), base_folder)
    
    download_folder = os.path.join(unique_folder, 'download')
    output_folder = os.path.join(unique_folder, 'output')

    os.makedirs(download_folder, exist_ok=True)
    os.makedirs(output_folder, exist_ok=True)

    return unique_folder, download_folder, output_folder

# Function to generate a PDF from a URL screenshot
def generate_pdf_from_url(download_folder, url, include_footer, store_name, visit_ref):
    logging.info(f"Generating PDF from URL: {url}")
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--no-sandbox")
    # chrome_options.add_argument("--disable-dev-shm-usage")
    
    # Automatically install and setup ChromeDriver
    # chromedriver_autoinstaller.install()

    try:
        driver = webdriver.Chrome(options=chrome_options)
    except Exception as e:
        logging.error(f"Failed to initialize Chrome WebDriver: {e}")
        raise

    try:
        driver.get(url)
        time.sleep(30)  # Wait for the page to load fully

        # Disable overflow and dynamic behavior for screenshots
        driver.execute_script("document.body.style.overflow = 'hidden';")

        # Get dynamic dimensions of the page
        # total_height = driver.execute_script("return document.body.scrollHeight")
        total_height = driver.execute_script("return Math.max(document.body.scrollHeight, document.documentElement.clientHeight, document.documentElement.offsetHeight);")
        total_width = driver.execute_script("return Math.max(document.body.scrollWidth, document.documentElement.scrollWidth);")
        
        # Set the window size dynamically based on actual dimensions of the webpage
        driver.set_window_size(total_width, total_height)

        # Take screenshot after setting the window size
        screenshot = driver.get_screenshot_as_png()

        # Save the screenshot as an image using PIL
        screenshot_image = Image.open(io.BytesIO(screenshot))
        timestamp = int(time.time())
        screenshot_file_path = os.path.join(download_folder, f"Generated_pdf_{timestamp}.png")
        screenshot_image.save(screenshot_file_path)

        # If footer is to be included, we calculate the height needed
        footer_height = 40 if include_footer else 0
        pdf_height = total_height + footer_height

        # Create PDF from the image
        pdf_file_path = os.path.join(download_folder, f"Generated_pdf_{timestamp}.pdf")
        canvas_obj = canvas.Canvas(pdf_file_path, pagesize=(total_width, pdf_height))
        canvas_obj.drawImage(screenshot_file_path, 0, footer_height, width=total_width, height=total_height)
        canvas_obj.save()

    except Exception as e:
        logging.error(f"Error during PDF generation: {e}")
        driver.quit()
        raise
    driver.quit()
    logging.info("PDF generated successfully")
    return pdf_file_path

@app.route('/generate_pdf', methods=['POST'])
def generate_pdf():
    # Copy the request context into the thread
    @copy_current_request_context
    def process_pdf():
        try:
            url = request.json.get('url')
            include_footer = request.json.get('include_footer', True)
            store_name = request.json.get('store_name', 'store')
            visit_ref = request.json.get('visit_ref', 'visit')

            if not url:
                return jsonify({'error': 'URL not provided in the request body'}), 400

            # Create unique directories for each request
            unique_folder, download_folder, output_folder = setup_unique_directories(store_name, visit_ref)

            hex_color = '#0000FF'  # Example hex color for blue ribbon
            lower_hsv = np.array([90, 50, 50])  # Example lower HSV bound for blue
            upper_hsv = np.array([130, 255, 255])  # Example upper HSV bound for blue

            # Generate and split the PDF
            pdf_file_path = generate_pdf_from_url(download_folder, url, include_footer, store_name, visit_ref)
            pdf_output_path = os.path.join(output_folder, os.path.basename(pdf_file_path))
            
            # Call split_pdf_by_ribbon_color to add footer and split by ribbon color
            split_pdf_by_ribbon_color(pdf_file_path, pdf_output_path, output_folder, lower_hsv=lower_hsv, upper_hsv=upper_hsv, store=store_name, visit_ref=visit_ref)

            # Schedule cleanup of the unique directory after 20 seconds
            threading.Thread(target=cleanup_directory, args=(unique_folder,)).start()

            return send_file(pdf_output_path, as_attachment=True)

        except Exception as e:
            error_message = f"An error occurred: {str(e)}"
            logging.error(error_message)
            return jsonify({'error': error_message}), 500

    # Submit the PDF generation process to a thread and return the result
    future = executor.submit(process_pdf)
    return future.result()

if __name__ == '__main__':
    logging.info("Starting the Flask server...")
    app.run(debug=True, port=4210, host='0.0.0.0')
>>>>>>> e0b73e296f8de5f2d6c99982e4166cce87d582c8
