import fitz  # PyMuPDF
import numpy as np
import cv2
from PIL import Image
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
import os

# Function to convert hex color to HSV range
def hex_to_hsv(hex_color):
    hex_color = hex_color.lstrip('#')
    rgb = tuple(int(hex_color[i:i+2], 16) for i in (0, 2, 4))
    rgb = np.uint8([[rgb]])
    hsv = cv2.cvtColor(rgb, cv2.COLOR_RGB2HSV)[0][0]
    return hsv

# Function to add padding to an image
def add_padding(image, padding=50):
    h, w, _ = image.shape
    padded_image = np.full((h + padding, w, 3), 255, dtype=np.uint8)  # Create a white padded image
    padded_image[:h, :] = image  # Add padding below the image content
    return padded_image

# Function to split the PDF by detecting a ribbon color
def split_pdf_by_ribbon_color(pdf_path, pdf_output_path, output_folder, hex_color=None, lower_hsv=None, upper_hsv=None, padding=50, store="", visit_ref=""):
    if hex_color:
        hsv_color = hex_to_hsv(hex_color)
        lower_hsv = np.array([hsv_color[0] - 10, 50, 50])
        upper_hsv = np.array([hsv_color[0] + 10, 255, 255])
    elif lower_hsv is None or upper_hsv is None:
        raise ValueError("Either hex_color or both lower_hsv and upper_hsv must be provided.")
    
    pdf_document = fitz.open(pdf_path)
    page = pdf_document[0]
    pix = page.get_pixmap()

    # Convert Pixmap to a PIL Image
    image = Image.frombytes("RGB", [pix.width, pix.height], pix.samples)

    # Convert PIL Image to OpenCV image
    image = np.array(image)
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

    # Detect the ribbon using color detection
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, lower_hsv, upper_hsv)
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    split_points = []
    image_width = image.shape[1]
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        if w > image_width * 0.9 and h > 10:
            split_points.append(y)

    split_points = sorted(set(split_points))

    splits = []
    prev_point = 0
    page_number = 1
    for point in split_points:
        if point > prev_point:
            split_img = image[prev_point:point, :]
            padded_img = add_padding(split_img, padding)
            split_pil = Image.fromarray(cv2.cvtColor(padded_img, cv2.COLOR_BGR2RGB))
            splits.append((split_pil, page_number))
            page_number += 1
            prev_point = point

    split_img = image[prev_point:, :]
    padded_img = add_padding(split_img, padding)
    split_pil = Image.fromarray(cv2.cvtColor(padded_img, cv2.COLOR_BGR2RGB))
    splits.append((split_pil, page_number))
    
    # Save the split images as PNG files and then combine them into a PDF with A4 pages
    png_files = []
    for idx, (split, page_number) in enumerate(splits):
        png_path = os.path.join(output_folder, f'split_image_{idx}.png')
        split.save(png_path)
        png_files.append((png_path, page_number))

    # Create the PDF with each image on a separate A4-sized page, with footer at bottom of page
    c = canvas.Canvas(pdf_output_path, pagesize=A4)
    footer_height = 5  # Height of the footer space
    
    for png_file, page_number in png_files[:-1]:
        pil_image = Image.open(png_file)
        img_width, img_height = pil_image.size

        # Calculate scaling factor to fit the image within A4 width while keeping aspect ratio
        scale = min(A4[0] / img_width, (A4[1] - footer_height) / img_height)
        img_width_scaled = img_width * scale
        img_height_scaled = img_height * scale

        # Position the image at the top of the page with scaling applied
        x_position = (A4[0] - img_width_scaled) / 2  # Center horizontally
        y_position = A4[1] - footer_height - img_height_scaled  # Position image with footer space below

        # Draw the scaled image on the PDF canvas
        c.drawImage(png_file, x_position, y_position, width=img_width_scaled, height=img_height_scaled)

        # Draw footer text at the bottom of the page
        footer_text_left = f"Page {page_number} {store}"
        footer_text_right = f"Visit Ref: {visit_ref}"
        left_margin = 30
        right_margin = A4[0] - 150  # Adjust to fit within page width
        footer_y_position = 15  # Position footer near the bottom

        c.setFont("Helvetica", 8)
        c.drawString(left_margin, footer_y_position, footer_text_left)
        c.drawString(right_margin, footer_y_position, footer_text_right)

        c.showPage()  # Start a new page for each image

    c.save()  # Save the final PDF document

    print(f'Split into {len(splits)} images and saved to {pdf_output_path}.')
<<<<<<< HEAD


=======
>>>>>>> e0b73e296f8de5f2d6c99982e4166cce87d582c8
